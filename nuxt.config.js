export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'UI/UX Designer & Developer | Peter Garcia',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Personal Portfolio for Peter Garcia, UI/UX Designer and Developer' },
      { name: 'format-detection', content: 'telephone=no' },
      {property: 'og:image', content: '/bricksandcanvas.png'},
      {property: 'og:description', content: 'BricksandCanvas | Personal Portfolio for Peter Garcia, UI/UX Designer and Developerg'},
      {property: 'og:type', content: 'website'}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/semantic.min.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['@/plugins/aos.client', '@/plugins/prlx'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/pwa',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
