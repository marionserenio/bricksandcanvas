<template>
<div>
    <Header />
    <div class="case-studies cebupacific">
        <div class="hero">
            <div class="ui container">
                <div class="ui grid stackable">
                    <div class="ten wide column">
                        <div class="top-heading" data-aos-duration="1000" data-aos-delay="200" data-aos="fade-down" >Case Studies </div>
                        <h1 data-aos="fade-down"  data-aos-duration="1000" data-aos-delay="500">Queens Coffee Headless Commerce.</h1>
                        <p data-aos="fade-left"  data-aos-duration="1200" data-aos-delay="600">Developing the full online branding of the company as well as using Headless Ecommerce with Shopify Plus.</p>
                    </div>
                    <div class="six wide column">

                    </div>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="ui container">
                <div class="ui grid stackable">
                    <div class="four wide column case-study-content">
                        <div class="sticky">
                            <h6>CLIENT</h6>
                            <ul>
                                <li>Queens Coffee</li>
                            </ul>
                            <h6>ROLE</h6>
                            <ul>
                                <li>Branding</li>
                                <li>User Interface Design</li>
                                <li>High Fidelity Designs</li>
                                <li>Backend Development</li>
                                <li>Front-end Development</li>
                            </ul>

                            <h6>TECHNOLOGIES</h6>
                            <ul>
                                <li>Adobe XD</li>
                                <li>Adobe Photoshop</li>
                                <li>Adobe Illustrator</li>
                            </ul>
                        </div>
                    </div>
                    <div class="twelve wide column case-study-images">
                        <h2>I love coffee, and I loved working on this project</h2>
                        <p>I started my career as a Graphic Designer creating logos and posters. After a certain time, I deviated to Web Design and UI/UX. When I was tasked to develop the whole direction for Queens Coffee.  I took it upon myself to create an aesthetic that I will be proud of, and other people will see as a great brand.</p>

                        <div class="images-container">
                            <picture data-aos="fade-up" data-aos-duration="800" data-aos-delay="800">
                                <source srcset="@/assets/casestudies/queenscoffee/queenscoffee_.webp" type="image/webp">
                                <img src="@/assets/casestudies/queenscoffee/queenscoffee_.png" alt="Queens Coffee Logo">
                            </picture>
                            <picture data-aos="fade-up" data-aos-duration="800" data-aos-delay="800">
                                <source srcset="@/assets/casestudies/queenscoffee/queenscoffee_products.webp" type="image/webp">
                                <img src="@/assets/casestudies/queenscoffee/queenscoffee_products.png" alt="Queens Coffee Logo">
                            </picture>
                            <picture data-aos="fade-up" data-aos-duration="800" data-aos-delay="800">
                                <source srcset="@/assets/casestudies/queenscoffee/queenscoffee_homepage.webp" type="image/webp">
                                <img src="@/assets/casestudies/queenscoffee/queenscoffee_homepage.png" alt="Queens Coffee Logo">
                            </picture>
                            <picture data-aos="fade-up" data-aos-duration="800" data-aos-delay="800">
                                <source srcset="@/assets/casestudies/queenscoffee/queenscoffee_product.webp" type="image/webp">
                                <img src="@/assets/casestudies/queenscoffee/queenscoffee_product.png" alt="Queens Coffee Logo">
                            </picture>
                            <picture data-aos="fade-up" data-aos-duration="800" data-aos-delay="800">
                                <source srcset="@/assets/casestudies/queenscoffee/queenscoffee_collections.webp" type="image/webp">
                                <img src="@/assets/casestudies/queenscoffee/queenscoffee_collections.png" alt="Queens Coffee Logo">
                            </picture>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <Footer />
</div>

</template>
<style scoped lang="scss">
  $gold: #DFD3C3;
  $lightgold: #AAA195;

  .case-studies{
      background:#000;
      .top-heading{
        color:$lightgold;
        font-family: 'Inter', sans-serif;
      }
      .hero{
          height:calc(100vh - 120px);
          display: flex;
          align-content: center;
          justify-content: center;
          align-items: center;
          color:$gold;
          h1{
            font-family: 'DM Serif Display', serif;
            font-size:82px;
            line-height: 78px;
            color:$gold;
            margin-bottom:30px;
          }
          p{
              color:#fff;
              font-family: 'Inter', sans-serif;
              font-size:15px;
              line-height: 26px;
              margin-left:120px;
              color:$lightgold;
          }
      }
      .main-content{
        color:#fff;
        padding:120px 0px;
        .case-study-content{
            .sticky{
                position: -webkit-sticky;
                position: sticky;
                top: 20px;
            }
            h6{
                font-family: 'Inter', sans-serif;
                font-size:12px;
                letter-spacing: 3px;
                margin-bottom:10px;
                color:$gold;

            }
            ul{
                margin:0px;
                font-family: 'Inter', sans-serif;
                padding:0px;
                list-style: none;
                li{
                    color:$lightgold;
                    line-height: 24px;
                    font-size:11px;
                }
            }
        }
        .case-study-images{
            h2{
                font-family: 'DM Serif Display', serif;
                font-size:40px;
                color:$gold;
            }
            p{
                font-family: 'Inter', sans-serif;
                font-size:16px;
                color:$lightgold;
                line-height: 28px;
            }
            .images-container{
                img{
                    max-width: 100%;
                }
            }
        }
      }
  }
  @media (max-width: 768px) and (min-width: 240px) {
            .mobile-flip{
                display:flex;
                flex-wrap: wrap;
                flex-direction: row;
                .text-container{
                    order:1;
                }
                .img-container{
                    order:2;
                }
            }
        .case-studies{
            .hero{
                height:80vh;
                .top-heading{
                    margin-top:120px;
                }
                h1{
                    font-size:62px;
                    line-height:52px;
                }
                p{
                    margin-left:0px !important;
                }
                img{
                    display:none;
                }
            }
            .main-content{
                padding:40px 0px;
                .ui.grid{
                    display:flex;
                    flex-wrap: wrap;
                    flex-direction: row;
                    .four.wide.column{
                        order:2;
                    }
                    .twelve.wide.column{
                        order:1;
                        h2{
                            font-size:32px;
                        }
                    }
                }
            }
        }
    }
</style>
